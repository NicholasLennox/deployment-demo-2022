package no.accelerate.deploymentdemo2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeploymentDemo2022Application {

    public static void main(String[] args) {
        SpringApplication.run(DeploymentDemo2022Application.class, args);
    }

}
