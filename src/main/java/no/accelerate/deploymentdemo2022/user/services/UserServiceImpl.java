package no.accelerate.deploymentdemo2022.user.services;

import no.accelerate.deploymentdemo2022.user.exceptions.UserNotFoundException;
import no.accelerate.deploymentdemo2022.user.models.AppUser;
import no.accelerate.deploymentdemo2022.user.repositories.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public AppUser findById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User doesnt exist with id " + id));
    }

    @Override
    public Collection<AppUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public AppUser add(AppUser user) {
        return userRepository.save(user);
    }

    @Override
    public void update(AppUser user) {
        userRepository.save(user);
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }
}
