package no.accelerate.deploymentdemo2022.user.services;

import no.accelerate.deploymentdemo2022.user.models.AppUser;

import java.util.Collection;

public interface UserService {
    AppUser findById(int id);
    Collection<AppUser> findAll();
    AppUser add(AppUser user);
    void update(AppUser user);
    void delete(int id);
}
