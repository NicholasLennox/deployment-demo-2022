package no.accelerate.deploymentdemo2022.user.repositories;

import no.accelerate.deploymentdemo2022.user.models.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Integer> {
}
