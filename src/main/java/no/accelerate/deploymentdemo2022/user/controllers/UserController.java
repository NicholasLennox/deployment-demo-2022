package no.accelerate.deploymentdemo2022.user.controllers;

import no.accelerate.deploymentdemo2022.user.models.AppUser;
import no.accelerate.deploymentdemo2022.user.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping(value = "api/v1/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("{id}")
    public ResponseEntity findOne(@PathVariable int id){
        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping
    public ResponseEntity add(@RequestBody AppUser user){
        int userId = (userService.add(user)).getId();
        URI uri = URI.create("api/v1/users/" + userId);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable int id, @RequestBody AppUser user){
        if(id != user.getId())
            return ResponseEntity.badRequest().build();
        userService.update(user);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
